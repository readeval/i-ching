;;; for standalone file usage without ASDF / Quicklisp
; This is the Standalone file option without ASDF / QUICKLISP dependency - i-c.lisp
;
;usage : load i-c.lisp, when developing, develop in i-ching.lisp, copy to this file
;and uncomment no-package and in-package lines
;TODO - better documentation / instructions for standalone option


(defpackage :i-ching
  (:use #:cl)
  (:nicknames "i-c" :i-c)
  (:export :i-c :hexagram :pictogram))


(in-package :i-ching)

(load "yarrow-algorithm.lisp")
(load "hexagrams.lisp")

