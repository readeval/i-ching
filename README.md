# CL I-Ching
I-Ching terminal divination with yarrow algorithm. Hexagram, trigram info lookup and match to digits.

PRE-ALPHA, not all features are in. 

# Overview

``` 
I-CHING> (i-c:i-c)
 0 =   =  Broken (Yin 陰)  Unchanging [line 6] 
 0 =   =  Broken (Yin 陰)  Unchanging [line 5] 
 1 =====  Solid  (Yang 陽) Unchanging [line 4] 
 1 ==!==  Solid  (Yang 陽)   Changing [line 3] 
 0 =   =  Broken (Yin 陰)  Unchanging [line 2] 
 1 ==!==  Solid  (Yang 陽)   Changing [line 1] 

Drawn digits:   (1 0 1 1 0 0) 101100 : Hexagrams are drawn from the bottom line 
Inner trigram:  (1 0 1) 離 Lí : Heart of Fire ( 火 Huǒ ) ☲ 
Outer trigram:  (1 0 0) 震 Zhèn : Thunder ( 雷 Léi) ☳ 
Hexagram:   ䷶  101100 
Description:  55. Abounding (豐 fēng); Abundance; Goal Reached Ambition Achieved 
```

Features: 
- Cast / divinate a Hexagram with a Yarrow algorithm. Built out of text lines with changing 
  / unchanging status
- Cast a Hexagram and get a text description with a pictogram, trigrams 
- Get a pictogram / symbol / Unicode symbol based on 6 or 3 digit strings or lists
- (In-progress) Enter a 6-digit string or list for a Hexagram or 3 digits for a Trigram and get a description

# Usage

- Yarrow Divination with text data (i-c:i-c) 
- Hexagram only without text data  (i-c:hexagram)
- Pictogram based on digits, hexagram and trigram (i-c::pictogram "111000") 

# Quickstart

- with ASDF or Quicklisp, currently manual install:

Clone the directory to the appropirate Quicklisp or ASDF directory
and load i-ching

```
(ql:quickload "i-ching")
(i-c:i-c)
```

- From file: in REPL, execute in the path of downloaded directory i-ching: 

``` common-lisp
(load "i-c.lisp") 
(in-package :i-ching)
(i-c)
;other commands
```

# Detailed use info

To just divinate/draw some yarrow and print a hexagram without additional info text
``` common-lisp
(i-c:hexagram) 
 ```

To generate pictograms based on a list or a string.

``` common-lisp
(i-c:pictogram "000")
(i-c:pictogram '(1 1 1))
(mapcar #'i-c:pictogram '("000" "111" "000111" )
(mapcar #'i-c:pictogram '((0 0 0) (0 0 0) (0 0 0 0 0 1)))
```

# Beginner details - file:

Install if needed:  Git and Quicklisp. The following instructions are for a *nix system:

Clone with git 

```
cd ~/quicklisp/local-projects
git clone https://gitlab.com/readeval/i-ching
```

Execute in the Lisp REPL:
``` common-lisp
(ql:quickload "i-ching")
(i-c:hexagram)
;other commands
```

If, for any reason, you want to play with the program without Quicklisp / ASDF,
just (load "i-c.lisp") in the REPL. 

# Style and concept
- Should be stand-alone without dependence on external libraries
- No macros unless absolutely necessary
- As little OOP as possible
- TODO - cleanup, optimization, features and refactoring.

# Some notes of interest
- Traditionally, a hexagram is divinated from the bottom, first "digit" being the bottom
  line. So a digit string of "000111" would correspond to the hexagram that looks like pants,
  also known as ䷋ "HEXAGRAM FOR STANDSTILL, 19915 4DCB " 

# Tested on:
SBCL, CLISP, GCL 

# TODO
- Add changing lines information
- Add 2 coin and 3 coin method (inferior)
- Add comparison / counter of probabilities for the 3 methods 
- Add matching trigrams / hexagrams 
- Add better hexagram descriptions from Wikipedia ( no copyright issues )
- Test on more Operating Systems /compilers etc 
- Publish on Ultralisp and Quicklisp (when out of pre-alpha)
- Add testing 
- Check text info data. The developer of this application does not know the I-ching 
  nor Chinese, so careful comparison and checking is needed.
- Check yarrow divination and line drawing. 
- Add integration of user typing, mousing & other computer usage for magic 
  smoke random seed generation.

# How can You help?
- Submit bug reports & feedback.
- Written on a GNU Debian machine. Feedback from other OS users welcome.
- Add / correct the text database from credible Free information sources. 
- Information should be copyleft / permissible for reuse. 

# License
See LICENSE.md for detailed information about included code. 

Port of Yarrow algorithm to CL based on Javascript algorithm by Brian Fitzgerald
https://github.com/Brianfit/I-Ching

Common Lisp code 
Copyright (c) readeval 2020

Licensed under the GNU AGPL License. 
