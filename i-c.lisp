;;;; I-ching REPL oracle with Yarrow algorithm divination.
;;;; Print and lookup hexagrams from strings or Yarrow algo drawing.
;;;; Implemented in Common Lisp
;
; Main model
; Yarrow Divination (digit generation )-> Information Lookup (match digits to text database)-> Print
; Or: Digit match -> Information Lookup -> Print
;
; To draw / consult the oracle & get detailed info :
; (i-c::i-c)
;
; To just divinate / draw yarrow and print a hexagram without additional info text:
; (i-c::hexagram)
;
; Generate pictograms based on a list, a string, or a list of strings or lists:
;
; (i-c::pictogram "000")
; (i-c::pictogram '(1 1 1))
; (mapcar #'i-c::pictogram '("000" "111" "000111"))

;;; For standalone without ASDF/ Quicklisp, use i-c.lisp, which has the following uncommented.
(load "no-package.lisp"); for no-package use only

(in-package :i-ching)

(defvar *trigrams*) ; text db in vector for trigrams;! style - allways *earmuff* globals!
(defvar *hexagrams*); text db in vector for hexagrams
(defvar *changing-lines*) ; store ascii for printing

;;; TODO refactor with defstruct ?

(defvar *hexagram-data*) ; temporary hexagram casting holder
(defvar *trigram-1*) ; list with first trigram
(defvar *trigram-2*) ; list with second trigram
(defvar *hexagram-digits*); list with hexagram digits
(defvar *hexagram-six-string*); string with hexagram digits

;;; A simple hexagram yarrow draw and print, without info DB lookup

(defun hexagram ()
  "Divine with Yarrow and Print a Hexagram of six lines with  changing / unchanging lines"
  (loop for i from 1 to 6
        for y = (yarrow-line-cast); function in yarrow-algorithm.lisp
        do (format *standard-output* "~& ~A ~%" y)))

;;; Accessory functions

;;; Printing  and conversions

(defun concat-list-to-string (lst)
  "Takes a list of separate integers and joins them into a string"
  (format nil "~{~A~}" lst))

(defun string-to-list (str)
  "convert numbers string to list with individual integers"
  (loop for i from 0 to (1- (length str))
        collect (parse-integer (string (elt str i)))))

(defun list-split-print (list)
  "split list and format print"
  (cond ((null list) nil)
        (t (format *standard-output* "~A " (car list))
           (list-split-print (rest list)))))

(defun print-to-screen (&rest values)
  "prints given values to screen, with new lines for lists"
  (loop for i in values
        if (listp i)
          do (terpri (list-split-print i))
        else do (format *standard-output* "~A" i)))

(defun format-print-line (list)
  "print string for list like (0 NIL)  to changing / unchanging strong / weak line"
  (cond
    ((equal list '(0 T))  "0 == ==  Broken (Yin  陰)   Changing")
    ((equal list '(1 T))  "1 ==!==  Solid  (Yang 陽)   Changing")
    ((equal list '(1 NIL))"1 =====  Solid  (Yang 陽) Unchanging")
    ((equal list '(0 NIL))"0 =   =  Broken (Yin  陰) Unchanging")
    (t "Wrong input in format-print-line")))

(defun print-changing-lines (list)
  "prints changing lines as ascii"
  (loop for i in (reverse list) ; hexagrams are drawn from bottom line
        for y downfrom 6
        do (format *standard-output* "~& ~A [line ~A] ~%" (format-print-line i) y)))

;;; Data processing functions & helpers

;;; text match and return functions

(defun find-position-in-db (x &optional (db *trigrams*))
  "finds x position in text info database file, *trigrams* default"
  (position x db :test 'equal)) ;text info in hexagrams.lisp

(defun get-text (x &optional (offset 0) (db *trigrams*))
  "gets text field based on offset and reference position"
  ;; trigram - offset 1 for description ; offset 2 for pictogram
  (aref db (+ offset (find-position-in-db x db))))

(defun check-input (x)
  "checks input x, a string or list, for length of 3 or 6 numbers"
  (assert (or (and (stringp x) (or (= (length x) 3)
                                   (= (length x) 6)))
              (and (consp x) (or (= (length x) 3)
                                 (= (length x) 6))))
          (x)
          "string or list containing three or six numbers only. digits: 1 or 0")
  x)

(defun one-or-zero-char-p (char)
  "checks if character is 1 or 0"
  (and (characterp char) (or (char= char #\1)
                             (char= char #\0))))

(defun one-or-zero-integer-p (int)
  "checks if integer is 1 or 0"
  (and (integerp int) (or (= int 1)
                          (= int 0))))

(defun one-or-zero-string-p (string)
  "checks if string contains only 1 or 0"
  (every #'one-or-zero-char-p string))

(defun one-or-zero-list-p (list)
  "checks if list contains only 1 or 0"
  (every #'one-or-zero-integer-p list))

;;; Divination / i-ching hexagram and trigram data function
;;; Returns a variety of divination data lists and strings.

(defun yarrow-divination-lists ()
  "calls line-cast 6 times and makes lists with hexagram and trigram strings and numbers "
  (loop  for i from 1 to 6
         for y = (yarrow-line-cast 'binary-line); yarrow-algorithm.lisp
         for x = (car y)
         collect y into hexagram
         collect x into hexagram-digits
         when (<= i 3)
           collect y into first-trigram
           and collect x into first-trigram-digits
         else
           collect y into second-trigram
           and collect x into second-trigram-digits
         finally (return (list
                          first-trigram-digits
                          second-trigram-digits
                          hexagram-digits
                          hexagram
                          first-trigram
                          second-trigram
                          (concat-list-to-string hexagram-digits)))))

(defun hexagram-data ()
  "provides data from hexagram draw as a list with lists and strings"
  (setf *hexagram-data* (yarrow-divination-lists));all hexagram divination data
  (setf *trigram-1* (first *hexagram-data*))
  (setf *trigram-2* (second *hexagram-data*))
  (setf *hexagram-digits* (third *hexagram-data*))
  (setf *changing-lines* (fourth *hexagram-data*))
  (setf *hexagram-six-string* (elt *hexagram-data* 6)))


(defun pictogram (x)
  "returns pictogram representing hexagram or trigram based on numbers in x"
  (when (check-input x)
    (cond
      ((and (one-or-zero-list-p x) (= 3 (length x)))
       (get-text x 2))
      ((and (one-or-zero-string-p x) (= 3 (length x)))
       (get-text (string-to-list x) 2))
      ((and (= 6 (length x)) (one-or-zero-string-p x))
       (get-text x 2  *hexagrams*))
      ((and (= 6 (length x)) (one-or-zero-list-p x))
       (get-text (concat-list-to-string x) 2  *hexagrams*))
      (t "Wrong input for pictogram"))))

;;; Main divination function

;; TODO - add text lookup via string

(defun i-c (&optional (hex-info nil))
  "Divines hexagram from yarrow and prints text info data, including pictograms"
  (when (null hex-info)
    (hexagram-data))
  ;; add string look-up
  (print-changing-lines *changing-lines*)
  (print-to-screen
   nil
   (list "Drawn digits:  " *hexagram-digits* *hexagram-six-string*
         ":Hexagrams are drawn from the bottom line")
   (list "Inner trigram: "
         (get-text *trigram-1*)
         (get-text *trigram-1* 1)
         (get-text *trigram-1* 2))
   (list "Outer trigram: "
         (get-text *trigram-2*)
         (get-text *trigram-2* 1)
         (get-text *trigram-2* 2))
   (list "Hexagram: "
         (get-text *hexagram-six-string* 2 *hexagrams*)
         (get-text *hexagram-six-string* 0 *hexagrams*))
   (list "Description: "(get-text *hexagram-six-string* 1 *hexagrams*))
   (list "Long description: "(get-text *hexagram-six-string* 4 *hexagrams*))))
