;;;; package.lisp

(defpackage :i-ching
  (:use #:cl)
  (:nicknames "i-c" :i-c)
  (:export :i-c :hexagram :pictogram))
