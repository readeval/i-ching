;;;; i-ching.asd
(defsystem "i-ching"
  :description "I-Ching CL terminal divination with yarrow algorithm.Info lookup and digit to pictogram match. "
  :author "readeval https://gitlab.com/readeval/i-ching "
  :license  "GNU AGPL"
  :version "0.0.1"
  :serial t
  :components ((:static-file "README.md")
               (:static-file "LICENSE.md")
               (:file "package")
               (:file "yarrow-algorithm")
               (:file "i-ching")
               (:file "hexagrams")))
