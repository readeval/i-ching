;;;; Yarrow i-ching algorithm ported to Common Lisp, see original for detailed comments
;;;; original JS by Brian Fitzgerald https://github.com/Brianfit/I-Ching
;;;  This port needs to be made a lot more lispy

(in-package :i-ching)

(defvar *stalks* 50)
(defvar *hand-pile*)
(defvar *east-pile*)
(defvar *west-pile*) 
(defvar *east-remainder*)
(defvar *west-remainder*)
(defvar *count-value-1*)
(defvar *count-value-2*)
(defvar *count-value-3*)
(defvar *line-value*)
                      ;
(defun divide-stalks (stalks)
  "Divide stalks into eastpile, westpile, subtract one from westpile, put in handpile"
  (let* ((rand (make-random-state t)))
   (setf *west-pile* (1+ (random stalks rand)))
   (setf *east-pile* (- stalks *west-pile*))
   (setf *west-pile* (decf *west-pile*))
   (setf *hand-pile* 1)))

(defun divide-east-and-west()
  (setf *west-remainder* (mod *east-pile* 4))
  (setf *east-remainder* (mod *west-pile* 4))
  (when (= *east-remainder* 0)(setf *east-remainder* 4))
  (when (= *west-remainder* 0)(setf *west-remainder* 4))
  (setf *hand-pile* (+ *hand-pile* *east-remainder* *west-remainder*)))

;;; Draws a text line for the hexagram

(defun draw-line (line change)
  "Divinates one hexagrams line and puts out a text line "
  (cond ((and (eq line 'weak)   (eq change 'changing)) "0 == == Broken (Yin 陰)  Changing")
        ((and (eq line 'strong) (eq change 'changing)) "1 ==!== Solid  (Yang 陽) Changing")
        ((and (eq line 'strong) (null change))         "1 ===== Solid  (Yang 陽) Unchanging")
        ((and (eq line 'weak)   (null change))         "0 =   = Broken (Yin 陰)  Unchanging")
        (t "Wrong input in draw-line")))

;;; binary-line output option Added by readeval 

(defun binary-line (line change)
  "Makes a list with a binary digit and T for changing, NIL for unchanging"
  (cond ((and (eq line 'weak) (eq change 'changing)) '(0 t))
        ((and (eq line 'strong) (eq change 'changing)) '(1 t))
        ((and (eq line 'strong) (null change)) '(1 nil))
        ((and (eq line 'weak)   (null change)) '(0 nil))
        (t "Wrong input in binary-line")))

(defun yarrow-line-output (output)
  "outputs lines based on numbers from different algorithms"
  (cond
    ((= *line-value* 6) (funcall output 'weak 'changing));
    ((= *line-value* 7) (funcall output 'strong nil))
    ((= *line-value* 8) (funcall output 'weak nil))
    ((= *line-value* 9) (funcall output 'strong 'changing))
    (t "Wrong input in line-cast")))

;;; JS port continues
;;; TODO - clean-up yarrow-line-cast, especially output

(defun yarrow-line-cast (&optional (output 'draw-line))
  "cast yarrow & create text representation of hexagram line
   as broken or unbroken and changing or unchanging"

  (setf *stalks* 49) ; Remove one stalk and set it aside
  (divide-stalks *stalks*)
  (divide-east-and-west)
  (when (= (+ *east-remainder* *west-remainder* 1) 9)
    (setf *count-value-1* 2))
  (when (= (+ *east-remainder* *west-remainder* 1) 5)
    (setf *count-value-1* 3))
  (setf *stalks* (- *stalks* *hand-pile*))
  (divide-stalks *stalks*)
  (divide-east-and-west)
  (when (= (+ *east-remainder* *west-remainder* 1) 8)
    (setf *count-value-2* 2))
  (when (= (+ *east-remainder* *west-remainder* 1) 4)
    (setf *count-value-2*  3))
  (setf *stalks* (- *stalks* *hand-pile*))
  (divide-stalks *stalks*)
  (divide-east-and-west)
  (when (= 8 (+ *east-remainder* *west-remainder* 1))
    (setf *count-value-3*  2))
  (when (= 4 (+ *east-remainder* *west-remainder* 1))
    (setf *count-value-3*  3))
  (setf *line-value* (+ *count-value-1* *count-value-2*  *count-value-3*))
  (yarrow-line-output output))

;;;; End of JavaScript to Common Lisp port
;;;; rest following  by readeval 

;;;  Inferior algorithms - one coin, two coin, two-coin modified

;;; Modified two-coins method

(defun random-t-or-nil()
  "pseudo-random T if heads (1) or NIL if tails (0)"
  (plusp (random 2)))

; The two-coin method involves tossing one pair of coins twice:
; on the first toss, two heads give a value of 2, and anything else is 3;
; on the second toss, each coin is valued separately, to give a sum from 6 to 9, as above.
; This results in the same distribution of probabilities as for the yarrow-stalk method.
; Source: Wikipedia

(defun two-coins-first-toss ()
  "2 for both coins being heads (1), otherwise three"
  (cond ((and (random-t-or-nil) (random-t-or-nil)) 2); both heads
        (t 3))); else not both heads, return 3

(defun one-coin-toss-value ()
  "returns 2 for coin head, 3 for coin tail"
  (if (random-t-or-nil)
      2
      3))

(defun two-coins-second-toss ()
 "value each of the two coins separately, 2 for heads, 3 for tails. Sum result"
  (+ (one-coin-toss-value) (one-coin-toss-value)))

(defun two-coins-two-tosses ()
  "sum first and second toss. returns 6, 7, 8 or 9"
  (+ (two-coins-first-toss) (two-coins-second-toss)))

;;; Inferior algorithms

(defun divinate-with-two-coins(&optional (output 'draw-line))
  "returns lines from inferior algorithms, TODO"
  (setf *line-value* (two-coins-two-tosses))
  (yarrow-line-output output))
